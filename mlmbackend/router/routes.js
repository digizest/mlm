const express = require("express");
const Router = express.Router();
//user
const {
        createUser,
        createAdmin,
        deleteAdmin,
        deleteUser, 
        updateUserbyUser,
        updateUserbyAdmin,
        getUserlist, 
        getUserwithfilter, 
        getAnyUserdetail, 
        getanyAdmindetail
    } = require("../controller/user.controller");
//userType
const { 
    addNewusertype , 
    deleteUsertype , 
    getAllUsertype } = require("../controller/userType.controller")
//team
const { 
    addNewTeam,
    deleteTeam,
    updateTeam,
    getAllTeam,
    getAllTeamMembers,
} = require("../controller/team.controller")
//rank
const {
    addNewRank,
    deleteRank,
    getAllRank,
    updateRank,
    getAllTeamMembersByRank
} = require("../controller/rank.controller")
//user
Router.post("/createUser" , createUser );
Router.post("/createAdmin" , createAdmin );
Router.delete("/deleteUser/:uid" , deleteUser );
Router.delete("/deleteAdmin/:uid" , deleteAdmin );
Router.patch("/updateUserbyUser" , updateUserbyUser );
Router.patch("/updateUserbyAdmin" , updateUserbyAdmin);
Router.get("/getUserlist" , getUserlist );
Router.get("/getUserwithfilter" , getUserwithfilter );
Router.get("/getAnyUserdetail" , getAnyUserdetail );
Router.get("/getanyAdmindetail" , getanyAdmindetail );
//usertype
Router.post("/addNewusertype" , addNewusertype );
Router.delete("/deleteUsertype/:userTypeId" , deleteUsertype );
Router.get("/getAllUsertype" , getAllUsertype );
//team
Router.post("/addNewTeam" , addNewTeam );
Router.delete("/deleteTeam/:teamId" , deleteTeam );
Router.patch("/updateTeam" , updateTeam );
Router.get("/getAllTeam" , getAllTeam );
Router.get("/getAllTeamMembers" , getAllTeamMembers );
Router.patch("/updateTeam" , updateTeam );
//rank
Router.post("/addNewRank" , addNewRank );
Router.delete("/deleteRank/:rankId" , deleteRank );
Router.get("/updateRank" , updateRank );
Router.get("/getAllRank" , getAllRank );
Router.get("/getAllTeamMembersByRank/:rankName" , getAllTeamMembersByRank );

module.exports = Router