const db = require("../config/db");

exports.addNewRank = (req , res )=>{
    let {  rankName } = req.body;
    let rankData = [  rankName ]
    db.query("INSERT INTO `rank` ( `rankId` , `rankName` ) VALUES ?",
    [ [ rankData  ]],
    (err ,result)=>{
        if(err)
        {
            console.log("errrrrr",err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        console.log("result",result);
        return res.json({ msg : "Rank Added Successfully", status : 201})
    }
    )
}

exports.deleteRank = (req , res )=>{
    let rankId = req.params.rankId;
    db.query("DELETE FROM rank WHERE rankId = ?",
    [ rankId ],
    (err ,result)=>{
        if(err)
        {
            // console.log("errorrr" , err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        // console.log("result" , result);
        return res.json({ msg : "Rank deleted Successfully", status : 200})
    }
    )
}

exports.updateRank = (req , res )=>{
    let { rankName , rankId } = req.body;
    let data = [  rankName , rankId ]
    db.query("UPDATE `rank` SET `rankName`= ? WHERE rankId = ?",
    data ,
    (err ,result)=>{
        if(err)
        {
            console.log("errorrr" , err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        else{
            if(result.affectedRows)
            {
                return res.json({ msg : "Rank updated", status : 200})
            }
            else
            {
                return res.json({ msg : "Rank not found", status : 203})
            }
        }
    }
    )
}

exports.getAllRank = (req , res )=>{
    db.query("select * from rank",
    (err ,result)=>{
        if(err)
        {
            // console.log("errorrr" , err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        // console.log("result" , result);
        return res.json({ msg : "All Rank List", status : 200 , data : result})
    }
    )
}

exports.getAllTeamMembersByRank = (req , res )=>{
    let { rankName } = req.params;
    // console.log(rankName); 
    db.query("SELECT * FROM `user` WHERE rank = ?",
    [ rankName ],
    (err ,result)=>{
        if(err)
        {
            //console.log("errorrr" , err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        // console.log("result" , result);        
        return res.json({ msg : "All User List By Rank", status : 200 , data : result})
    }
    )
}
