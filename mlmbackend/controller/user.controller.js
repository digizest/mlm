const db = require("../config/db")

exports.createUser = (req , res )=>{
    let { name ,  email ,  dob ,  uid ,  mobile ,  password ,  sponsor ,  gender ,  rank ,  address ,  city ,  state ,  pincode ,  team ,  profileImage ,  nomineeName ,  nomineeRelation ,  nomineeAddress } = req.body;
    let userData = [ name ,  email ,  dob ,  uid ,  mobile ,  password ,  sponsor ,  gender ,  rank ,  address ,  city ,  state ,  pincode ,  team ,  profileImage ,  nomineeName ,  nomineeRelation ,  nomineeAddress ] 
    db.query("INSERT INTO  user ( name ,  email ,  dob ,  uid ,  mobile ,  password ,  sponsor ,  gender ,  rank ,  address ,  city ,  state ,  pincode ,  team ,  profileImage ,  nomineeName ,  nomineeRelation ,  nomineeAddress ) VALUES ?",
    [ [userData] ],
    (err ,result)=>{
        if(err)
        {
            console.log("errrrrr",err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        console.log("result",result);
        return res.json({ msg : "User Registered Successfully", status : 201})
    }
    )
}


exports.createAdmin = (req , res )=>{
    let { uid ,  adminName ,  email ,  password ,  admintype } = req.body;
    let adminData = [ uid ,  adminName ,  email ,  password ,  admintype ]
    db.query("INSERT INTO  admin ( uid ,  adminName ,  email ,  password ,  admintype ) VALUES ?",
    [ adminData ],
    (err ,result)=>{
        if(err)
        {
            console.log("errrrrr",err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        console.log("result",result);
        return res.json({ msg : "Admin Registered Successfully", status : 201})
    }
    )
}

exports.deleteUser = (req , res )=>{
    let uid = req.params.uid;
    db.query("DELETE FROM user WHERE uid = ?",
     [uid],
     (err , result)=>
     {
        if(err)
         {
            console.log("errrrrr",err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        console.log("result",result);
        return res.json({ msg : "User Deleted Successfully", status : 200})
     }
     )
}
// req = request
// res = response
exports.deleteAdmin = (req , res )=>{
    let uid = req.params.uid;
    db.query("DELETE FROM admin WHERE uid = ?",
    [ uid ],
    (err, result)=>
    {
        if(err)
        {
            console.log("errrrrr",err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        console.log("result",result);
        return res.json({ msg : "Admin Deleted Successfully", status : 200})
    }
    )
}

exports.updateUserbyUser = (req , res )=>{
    let data = []
    db.query("ccc",
    [data],
    (err, result)=>
    {
        if(err)
        {
            return res.json(err)
        }
        return res.json(result)
    }
    )
}

exports.updateUserbyAdmin = (req , res )=>{
    let data = []
    db.query("ccc",
    [ data ],
    (err, result)=>
    {
        if(err)
        {
            return res.json(err)
        }
        return res.json(result)
    }
    )
}

exports.getUserlist = (req , res )=>{
    db.query("SELECT * FROM user",
    (err, result)=>
    {
        if(err)
        {
              // console.log("errorrr" , err);
              return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }else{
            if(result.length == 0)
            {
                // console.log("result" , result);
                return res.json({ msg : "no User found", status : 203 })              
            }else{
                  // console.log("result" , result);
                  return res.json({ msg : "All User List", status : 200 , data : result})
            }
        }
    }
    )
}

exports.getUserwithfilter = (req , res )=>{
    let filter = []
    db.query("SELECT * FROM user",
    [ filter ],
    (err, result)=>
    {
        if(err){
            return res.json(err)
        }
        return res.json(result)
    }
    )
}

exports.getAnyUserdetail = (req , res )=>{
    let uid = req.params.uid
    db.query("SELECT * FROM user where uid = ?",
    [ uid ],
    (err, result)=>
    {
        if(err)
        {
              // console.log("errorrr" , err);
              return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }else{
            if(result.length == 0)
            {
                // console.log("result" , result);
                return res.json({ msg : "no team found", status : 203 })              
            }else{
                  // console.log("result" , result);
                  return res.json({ msg : "All Team List", status : 200 , data : result})
            }
        }    
    }
    )
}

exports.getanyAdmindetail = (res , req )=>{
    let uid = req.params.uid
    db.query("SELECT * FROM user where uid = ?",
    [ uid ],
    (err, result)=>
    {
        if(err)
        {
              // console.log("errorrr" , err);
              return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }else{
            if(result.length == 0)
            {
                // console.log("result" , result);
                return res.json({ msg : "no team found", status : 203 })              
            }else{
                  // console.log("result" , result);
                  return res.json({ msg : "All Team List", status : 200 , data : result})
            }
        }
    }
    )
}


exports.updatePassword = (res , req )=>{
    let { password , uid} = req.body;
    let data = [  password , uid ]
    db.query("UPDATE `user` SET `password`= ? WHERE `uid` = ?; ",
    [ data ],
    (err, result)=>
    {
        if(err)
        {
              // console.log("errorrr" , err);
              return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }else{
            if(result.length == 0)
            {
                // console.log("result" , result);
                return res.json({ msg : "no team found", status : 203 })              
            }else{
                  // console.log("result" , result);
                  return res.json({ msg : "All Team List", status : 200 , data : result})
            }
        }
    }
    )
}