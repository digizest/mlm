const db = require("../config/db");


exports.addNewTeam = (req , res )=>{
    let { teamName } = req.body;
    let teamData = [  teamName ]
    db.query("INSERT INTO `team` (  `teamName` ) VALUES ?",
    [ [ teamData] ],
    (err ,result)=>{
        if(err)
        {
            console.log("errrrrr",err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        console.log("result",result);
        return res.json({ msg : "Team Added Successfully", status : 201})
    }
    )
}

exports.deleteTeam= (req , res )=>{
    let teamId = req.params.teamId;
    db.query("DELETE FROM team WHERE teamId = ?",
    [ teamId ],
    (err ,result)=>{
        if(err)
        {
            // console.log("errorrr" , err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        // console.log("result" , result);
        return res.json({ msg : "Team deleted Successfully", status : 200})
        }
    )
}

exports.updateTeam = (req , res )=>{
    let { teamName , teamId } = req.body;
    let data = [ teamName , teamId ]
    db.query("UPDATE `team` SET `teamName`= ? WHERE teamId = ?",
    [ data ],
    (err ,result)=>{
        if(err)
        {
            // console.log("errorrr" , err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
            // console.log("result" , result);
            return res.json({ msg : "Team deleted Successfully", status : 200})
      }
    )
}

exports.getAllTeam = (req , res )=>{
    db.query("select * from team",
    (err ,result)=>{
        if(err)
        {
              // console.log("errorrr" , err);
              return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }else{
            if(result.length == 0)
            {
                // console.log("result" , result);
                return res.json({ msg : "no team found", status : 203 })              
            }else{
                  // console.log("result" , result);
                  return res.json({ msg : "All Team List", status : 200 , data : result})
            }
        }
      }
    )
}

exports.getAllTeamMembers = (req , res )=>{
    let { teamName } = req.params;
    db.query("SELECT * FROM `user` WHERE `team` = ?",
    [ teamName ] ,
    (err ,result)=>{
        if(err)
        {
            if(err)
            {
                //console.log("errorrr" , err);
                return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
            }
            else
            {
                if(result.length == 0 )
                {
                    // console.log("result" , result);        
                    return res.json({ msg : "no user found in this team", status : 203})
                }
                else
                {
                    // console.log("result" , result);        
                    return res.json({ msg : "All User List By Team", status : 200 , data : result})
                }
            }
        }
    })
}

