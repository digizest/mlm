const db = require("../config/db");


exports.addNewusertype = (req , res )=>{
    let {  userTypeName } = req.body;
    let data = [  userTypeName ]
    db.query("INSERT INTO usertype ( userTypeName) VALUES ?",
    [ [data ] ],
    (err ,result)=>{
        if(err)
        {
            console.log("errrrrr",err);
            return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }
        console.log("result",result);
        return res.json({ msg : "UserType Added Successfully", status : 201})
    }
    )
}

exports.deleteUsertype = (req , res )=>{
    let userTypeId = req.params.userTypeId;
    db.query("DELETE FROM usertype WHERE userTypeId = ?",
    [ userTypeId ],
    (err ,result)=>{
        if(err)
        {
              // console.log("errorrr" , err);
              return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }else{
            if(result.length == 0)
            {
                // console.log("result" , result);
                return res.json({ msg : "no usertype found", status : 203 })              
            }else{
                  // console.log("result" , result);
                  return res.json({ msg : "All usertype List", status : 200 , data : result})
            }
        }    

    }
    )
}

exports.getAllUsertype = (req , res )=>{
    db.query("select * from usertype",
    (err ,result)=>{
        if(err)
        {
              // console.log("errorrr" , err);
              return res.json({errCode : err.errno , msg : "something went wrong", status : 400})
        }else{
            if(result.length == 0)
            {
                // console.log("result" , result);
                return res.json({ msg : "no usertype found", status : 203 })              
            }else{
                  // console.log("result" , result);
                  return res.json({ msg : "All usertype List", status : 200 , data : result})
            }
        }    

    }
    )
}

