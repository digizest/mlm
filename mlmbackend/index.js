const express = require("express");
const app = express();
const routes = require("./router/routes")
const bodyParser = require("body-parser");
 // parse application/x-www-form-urlencoded
app.use(bodyParser.json({
    limit : "50mb"
}))


//  app.use(
//  bodyParser.urlencoded({
// limit: "50mb",
// extended: true,
//   parameterLimit: 50000,
// }));

app.use("/v1", routes);
app.listen(3200 , (err)=>{
    if(err)
    {
        console.log("error in running server on 3200", err);
    }
    else
    {
        console.log("server is up and running on port 3200");
    }
})
